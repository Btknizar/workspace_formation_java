package com.btk.business;

import java.math.BigDecimal;

public class Compte {

	private String numero;
	private String proprietaire;
	private BigDecimal solde;

	public Compte() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Compte(String numero, String proprietaire, BigDecimal solde) {
		super();
		this.numero = numero;
		this.proprietaire = proprietaire;
		this.solde = solde;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getProprietaire() {
		return proprietaire;
	}

	public void setProprietaire(String proprietaire) {
		this.proprietaire = proprietaire;
	}

	public BigDecimal getSolde() {
		return solde;
	}

	public void setSolde(BigDecimal solde) {
		this.solde = solde;
	}

	// méthode metier
	public void créditer(BigDecimal somme) {
		this.solde = this.solde.add(somme);
	}

	public void débiter(BigDecimal somme) {
		this.solde = this.solde.subtract(somme);
	}

}
