package com.btk;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		/***
		 * nizar
		 */
		

		try {

			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/bankdb";
			String user = "root";
			String password = "password";
			Connection conn = DriverManager.getConnection(url, user, password);

			Statement statement = conn.createStatement();

			ResultSet resultSet = statement.executeQuery("select * from comptes");
			

			while (resultSet.next()) {
				String num = resultSet.getString("Numero");
				String proprio = resultSet.getString("Proprietaire");
				BigDecimal solde = resultSet.getBigDecimal("Solde");
				System.out.println("Compte: " + num + " - " + proprio + " - " + solde);

			}
			
			conn.close();

		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Attention, il y a l'exception :" + e);
		}

	}
}
